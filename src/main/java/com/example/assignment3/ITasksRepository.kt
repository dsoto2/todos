package com.example.assignment3

import androidx.lifecycle.LiveData
import com.example.assignment3.ui.theme.Task

interface ITasksRepository {
    suspend fun getTasks(): List<Task>
    suspend fun deleteTask(task: Task)
    suspend fun addTask(task: Task)
    suspend fun toggleDone(task: Task)
}
